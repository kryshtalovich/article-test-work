<?php

declare(strict_types=1);

namespace App\Controller;

use App\ReadModel\Article\ArticleFetcher;
use App\Model\Article\UseCase\Create;
use App\Model\Article\UseCase\Edit;
use App\Model\Article\UseCase\Remove;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Работа со статьями
 * @Route("/article")
 */
abstract class ArticleController extends AbstractController
{
    /**
     * Получение всего списка статей
     * @Route("", name="get_all_articles", methods={"GET"})
     * @param ArticleFetcher $article
     * @return Response
     */
    abstract public function getAllArticles(ArticleFetcher $article): Response;

    /**
     * Получение статей конкретного пользователя
     * @Route("/author/{id}", name="article_filter", methods={"GET"})
     * @param $id
     * @param Request $request
     * @param ArticleFetcher $article
     * @return Response
     */
    abstract public function userArticles($id, Request $request, ArticleFetcher $article): Response;

    /**
     * Получение статьи по ее Id
     * @Route("/{id}", name="get_article", methods={"GET"})
     * @param $id
     * @param ArticleFetcher $article
     * @return Response
     */
    abstract public function getArticle($id, ArticleFetcher $article): Response;

    /**
     * Добавление новой статьи
     * @Route("", name="add_article", methods={"POST"})
     * @param Request $request
     * @param Create\Handler $handler
     * @return Response
     */
    abstract public function addArticle(Request $request, Create\Handler $handler): Response;

    /**
     * Редактирование статьи
     * @Route("/{id}", name="patch_article", methods={"PATCH"})
     * @param $id
     * @param Request $request
     * @param Edit\Handler $handler
     * @return Response
     */
    abstract public function patchArticle($id, Request $request, Edit\Handler $handler): Response;

    /**
     * Удаление статьи
     * @Route("/{id}", name="remove_article", methods={"DELETE"})
     * @param $id
     * @param Remove\Handler $handler
     * @return Response
     */
    abstract public function removeArticle($id, Remove\Handler $handler): Response;
}
