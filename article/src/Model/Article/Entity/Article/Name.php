<?php

declare(strict_types=1);

namespace App\Model\Article\Entity\Article;

use Doctrine\ORM\Mapping as ORM;
use Webmozart\Assert\Assert;

/**
 * @ORM\Embeddable
 */
class Name
{
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $first;
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $last;

    public function __construct(string $first, string $last)
    {
        Assert::notEmpty($first);
        Assert::notEmpty($last);

        $this->first = $first;
        $this->last = $last;
    }

    /**
     * Получение имени
     * @return string
     */
    public function getFirst(): string
    {
        return $this->first;
    }

    /**
     * Получение фамилии
     * @return string
     */
    public function getLast(): string
    {
        return $this->last;
    }

    /**
     * Получение ФИ
     * @return string
     */
    public function getFull(): string
    {
        return $this->first . ' ' . $this->last;
    }
}