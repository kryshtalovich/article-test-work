<?php

declare(strict_types=1);

namespace App\Model\Article\Entity\Article;

interface ArticleRepository
{
    public function get(Id $id): Article;

    public function add(Article $article): void;

    public function update(Article $article): void;

    public function remove(Article $article): void;
}