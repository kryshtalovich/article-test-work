<?php

declare(strict_types=1);

namespace App\Model\Article\Entity\Article;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="user_users", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"email"}),
 * })
 */
class User
{
    /**
     * @ORM\Column(type="user_user_id")
     * @ORM\Id
     */
    private $id;
    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable")
     */
    private $date;
    /**
     * @var Email|null
     * @ORM\Column(type="user_user_email", nullable=true)
     */
    private $email;
    /**
     * @var Name
     * @ORM\Embedded(class="Name")
     */
    private $name;


    private function __construct(Id $id, \DateTimeImmutable $date, Name $name)
    {
        $this->id = $id;
        $this->date = $date;
        $this->name = $name;
    }

    /**
     * Создание пользователя
     * lite версия, без пароля и тд
     * @param Id $id
     * @param \DateTimeImmutable $date
     * @param Name $name
     * @param Email $email
     * @return static
     */
    public static function create(Id $id, \DateTimeImmutable $date, Name $name, Email $email): self
    {
        $user = new self($id, $date, $name);
        $user->email = $email;
        return $user;
    }

    /**
     * Смена имени
     * @param Name $name
     */
    public function changeName(Name $name): void
    {
        $this->name = $name;
    }

    /**
     * Смена почты и имени
     * @param Email $email
     * @param Name $name
     */
    public function edit(Email $email, Name $name): void
    {
        $this->name = $name;
        $this->email = $email;
    }

    /**
     * Получение Id
     * @return Id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * Получение даты регистрации
     * @return \DateTimeImmutable
     */
    public function getDate(): \DateTimeImmutable
    {
        return $this->date;
    }

    /**
     * Получение почты
     * @return Email|null
     */
    public function getEmail(): ?Email
    {
        return $this->email;
    }

    /**
     * Получение имени
     * @return Name
     */
    public function getName(): Name
    {
        return $this->name;
    }
}