<?php

declare(strict_types=1);

namespace App\Model\Article\Entity\Article;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="article_article")
 */
class Article
{
    /**
     * @ORM\Column(type="article_article_id")
     * @ORM\Id
     */
    private $id;
    /**
     * @var \DateTimeImmutable
     * @ORM\Column(type="datetime_immutable")
     */
    private $date;
    /**
     * @var string
     * @ORM\Column(type="string", length=150, nullable=false)
     */
    private $title;
    /**
     * @var string
     * @ORM\Column(type="string", length=3500, nullable=false)
     */
    private $description;
    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", inversedBy="articles")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    private $author;

    public function __construct(Id $id, \DateTimeImmutable $date)
    {
        $this->id = $id;
        $this->date = $date;
    }

    /**
     * Получение Id статьи
     * @return Id
     */
    public function getId(): Id
    {
        return $this->id;
    }

    /**
     * Получение заголовка статьи
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * Установка заголовка статьи
     * @param string $title
     * @return Article
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Получение описания статьи
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * Установка описания статьи
     * @param string $description
     * @return Article
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Получение автора статьи
     * @return User
     */
    public function getAuthor(): User
    {
        return $this->author;
    }

    /**
     * Установка автора статьи
     * @param User $author
     * @return Article
     */
    public function setAuthor(User $author): self
    {
        $this->author = $author;
        return $this;
    }

    /**
     * Обновление даты статьи
     * @param \DateTimeImmutable $date
     * @return Article
     */
    public function updateDate(\DateTimeImmutable $date): self
    {
        $this->date = $date;
        return $this;
    }
}