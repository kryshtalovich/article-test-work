<?php

declare(strict_types=1);

namespace App\Model\Article\UseCase\Remove;

use App\Model\Article\Entity\Article\ArticleRepository;
use App\Model\Article\Entity\Article\Id;
use App\Model\Flusher;

class Handler
{
    private $article;
    private $flusher;

    public function __construct(ArticleRepository $article, Flusher $flusher)
    {
        $this->article = $article;
        $this->flusher = $flusher;
    }

    /**
     * Удаление статьи
     * @param Command $command
     */
    public function handle(Command $command): void
    {
        $article = $this->article->get(new Id($command->id));
        $this->article->remove($article);
        $this->flusher->flush();
    }
}