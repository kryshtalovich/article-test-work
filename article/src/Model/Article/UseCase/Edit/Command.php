<?php

declare(strict_types=1);

namespace App\Model\Article\UseCase\Edit;

class Command
{
    public $id;
    public $title;
    public $description;
    public $author;

    public function __construct(array $dataToEdit)
    {
        $this->id = $dataToEdit['id'];
        $this->title = $dataToEdit['title'];
        $this->description = $dataToEdit['description'];
        $this->author = $dataToEdit['author_id'];
    }
}