<?php

declare(strict_types=1);

namespace App\Model\Article\UseCase\Edit;

use App\Model\Article\Entity\Article\Id;
use App\Model\Article\Entity\Article\ArticleRepository;
use App\Model\Flusher;

class Handler
{
    private $article;
    private $flusher;

    public function __construct(ArticleRepository $article, Flusher $flusher)
    {
        $this->article = $article;
        $this->flusher = $flusher;
    }

    /**
     * Обновление данных в статье (заголовок, описание, автор)
     * @param Command $command
     */
    public function handle(Command $command): void
    {
        $article = $this->article->get(new Id($command->id));

        if ($command->title) {
            $article->setTitle($command->title);
        }
        if ($command->description) {
            $article->setDescription($command->description);
        }

        if ($command->author) {
            $article->setAuthor($command->author);
        }

        $article->updateDate(new \DateTimeImmutable());

        $this->article->update($article);

        $this->flusher->flush();
    }
}