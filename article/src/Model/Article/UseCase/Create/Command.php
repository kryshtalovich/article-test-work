<?php

declare(strict_types=1);

namespace App\Model\Article\UseCase\Create;

class Command
{
    public $title;
    public $description;
    public $author;

    public function __construct(array $data)
    {
        $this->title = $data['title'];
        $this->description = $data['description'];
        $this->author = $data['author_id'];
    }
}