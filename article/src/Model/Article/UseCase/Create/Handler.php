<?php

declare(strict_types=1);

namespace App\Model\Article\UseCase\Create;

use App\Model\Article\Entity\Article\Article;
use App\Model\Article\Entity\Article\ArticleRepository;
use App\Model\Article\Entity\Article\Id;
use App\Model\Article\Entity\Article\User;
use App\Model\Flusher;

class Handler
{
    private $article;
    private $flusher;

    public function __construct(ArticleRepository $article, Flusher $flusher)
    {
        $this->article = $article;
        $this->flusher = $flusher;
    }

    /**
     * Добавление новой статьи
     * @param Command $command
     */
    public function handle(Command $command): void
    {
        $article = new Article(
            Id::next(),
            new \DateTimeImmutable()
        );

        $article->setTitle($command->title);
        $article->setDescription($command->description);
        $article->setAuthor($command->author);

        $this->article->add($article);

        $this->flusher->flush();
    }
}