<?php

declare(strict_types=1);

namespace App\ReadModel\Article;

interface ArticleFetcher
{
    /**
     * Получение всех статей
     * @return array
     */
    public function getAllArticles(): array;

    /**
     * Поиск по id автора
     * @param $authorId
     * @return array|null
     */
    public function findByAuthorId($authorId): ?array;

    /**
     * Поиск по id статьи
     * @param $id
     * @return array|null
     */
    public function findByArticleId($id): ?array;
}