<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201217110701 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE article_article (id UUID NOT NULL, user_id UUID NOT NULL, date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, title VARCHAR(150) NOT NULL, description VARCHAR(3500) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_EFE84AD1A76ED395 ON article_article (user_id)');
        $this->addSql('COMMENT ON COLUMN article_article.date IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE user_users (id UUID NOT NULL, date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, email VARCHAR(255) DEFAULT NULL, name_first VARCHAR(255) NOT NULL, name_last VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F6415EB1E7927C74 ON user_users (email)');
        $this->addSql('COMMENT ON COLUMN user_users.date IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE article_article ADD CONSTRAINT FK_EFE84AD1A76ED395 FOREIGN KEY (user_id) REFERENCES user_users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE article_article DROP CONSTRAINT FK_EFE84AD1A76ED395');
        $this->addSql('DROP TABLE article_article');
        $this->addSql('DROP TABLE user_users');
    }
}
